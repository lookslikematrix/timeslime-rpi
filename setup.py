import subprocess
from setuptools import setup

VERSION = subprocess.check_output(["git", "describe", "--tags", "--dirty"]).strip().decode('utf-8')

def readme():
    """get readme content"""
    with open("README.md", encoding="utf-8") as f:
        return f.read()
setup(
    name="timeslime-rpi",
    version=str(VERSION),
    author="Christian Decker",
    author_email="christian.decker@lookslikematrix.de",
    license = "MIT",
    long_description=readme(),
    long_description_content_type="text/markdown",
    project_urls = {
        "Donate": "https://www.buymeacoffee.com/lookslikematrix",
        "Source Code": "https://gitlab.com/lookslikematrix/timeslime-rpi"
    },
    packages = ["timeslime_rpi"],
    install_requires=[
        "timeslime==1.5.4",
        "gpiozero==1.6.2",
        "rpi.gpio==0.7.1",
        "rpi-tm1637==1.3.4"
        ],
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.7',
    entry_points = {
        "console_scripts": [
            "timeslime-rpi = timeslime_rpi.__main__:main",
        ]
    }
)

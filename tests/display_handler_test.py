import unittest
from unittest.mock import Mock

from datetime import timedelta, datetime

from timeslime_rpi.handler import DisplayHandler

class TimeSpanSerializerTests(unittest.TestCase):
    def test_timedelta_to_display_positiv(self):
        # arrange
        display_handler = DisplayHandler()
        time_delta = timedelta(hours=2, minutes=3)

        # act
        [hours, minutes] = display_handler.timedelta_to_display(time_delta)

        # assert
        self.assertEqual(hours, 2)
        self.assertEqual(minutes, 3)

    def test_timedelta_to_display_negative(self):
        # arrange
        display_handler = DisplayHandler()
        daily_working_time = timedelta(hours=1)
        start_time = datetime.strptime('2021-09-01 06:00', '%Y-%m-%d %H:%M')
        stop_time = datetime.strptime('2021-09-01 08:30', '%Y-%m-%d %H:%M')

        # act
        [hours, minutes] = display_handler.timedelta_to_display(daily_working_time - (stop_time - start_time))

        # assert
        self.assertEqual(hours, 1)
        self.assertEqual(minutes, 30)

    def test_timedelta_on_property_change_None(self):
        # arrange
        on_property_change_mock = Mock()
        display_handler = DisplayHandler()
        display_handler.on_property_change = on_property_change_mock
        time_delta = timedelta(hours=2, minutes=3)

        # act
        display_handler.timedelta_to_display(time_delta)

        # assert
        on_property_change_mock.assert_called_with(2, 3)

    def test_timedelta_on_property_change_pre_set(self):
        # arrange
        on_property_change_mock = Mock()
        display_handler = DisplayHandler()
        display_handler.hours = 2
        display_handler.minutes = 3
        display_handler.on_property_change = on_property_change_mock
        time_delta = timedelta(hours=2, minutes=4)

        # act
        display_handler.timedelta_to_display(time_delta)

        # assert
        on_property_change_mock.assert_called_with(2, 4)

    def test_timedelta_on_property_no_change(self):
        # arrange
        on_property_change_mock = Mock()
        display_handler = DisplayHandler()
        display_handler.hours = 2
        display_handler.minutes = 3
        display_handler.on_property_change = on_property_change_mock
        time_delta = timedelta(hours=2, minutes=3)

        # act
        display_handler.timedelta_to_display(time_delta)

        # assert
        on_property_change_mock.assert_not_called()

if __name__ == '__main__':
    unittest.main()

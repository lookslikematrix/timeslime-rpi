# timeslime-rpi

## 1.3

* update timeslime to 1.5 which use UTC time

## 1.2

* update timeslime to 1.4 with support for [timeslime-server](https://gitlab.com/lookslikematrix/timeslime-server)

## 1.1

* display negative elapsed time as increasing time

* wait until a NTP server is connected

* bugfix: decrease CPU usage by using updating display only on changes

* bugfix: using own [tm1637](https://pypi.org/project/rpi-tm1637/) package

## 1.0

* initial version with simple button, 7-segment display and fixed working time
# timeslime-rpi

With *timeslime-rpi* you have the possibility to control your [**timeslime**](https://gitlab.com/lookslikematrix/timeslime/)

## Install

1. Install *timeslime-rpi*

    ~~~bash
    sudo apt install -y libdbus-1-dev libglib2.0-dev
    pip3 install --upgrade timeslime-rpi
    ~~~

2. Wire everything

    ![timeslime](./assets/timeslime.png)

3. Run *timeslime-rpi*

    ~~~bash
    timeslime init --weekly_hours 40
    timeslime-rpi
    ~~~

4. Add it to your autostart

    ~~~bash
    crontab -e
    # insert at the end
    @reboot /home/pi/.local/bin/timeslime-rpi
    ~~~

## Update

### 1.1 to 1.2

* timeslime was updated to version 1.4 so you have to update the timeslime database

    ~~~bash
    timeslime update
    ~~~
